package com.haud.dao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="simCard")
public class SimCard {
 
	@Id
	@GeneratedValue
    private Integer id;
    private Long imsi;
    private Integer msisdn;
    
    @ManyToOne()
    @JoinColumn(name="customer_id")
    private Customer customer;
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Long getImsi() {
		return imsi;
	}
	public void setImsi(Long imsi) {
		this.imsi = imsi;
	}
	public Integer getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(Integer msisdn) {
		this.msisdn = msisdn;
	}
	
	public Customer getCustomer() { 
		return customer;
	} 
	public void	setCustomer(Customer customer) { 
		this.customer = customer; 
	}
		
}
