package com.haud.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.haud.dao.SimCard;
import com.haud.dto.SimCardCreateRequestDto;
import com.haud.dto.SimCardCreateResponseDto;
import com.haud.dto.SimCardLinkResponseDto;
import com.haud.exception.CustomerNotFoundException;
import com.haud.exception.SimCardAlreadyLinkedException;
import com.haud.exception.SimCardNotFoundException;
import com.haud.services.SimCardService;
	
@RestController
@RequestMapping(path = "/sim")
public class SimCardController {

	@Autowired
    private SimCardService simCardService;
     
	Logger logger = LoggerFactory.getLogger(SimCardController.class);
	
    @PostMapping(path="", consumes = "application/json", produces = "application/json")
    public ResponseEntity<SimCardCreateResponseDto> createSimCard(@Valid @RequestBody SimCardCreateRequestDto simCardCreateRequestDto) {
    	logger.debug("Create Sim Card API called...");    	
    	SimCard simCard = simCardService.createSimCard(simCardCreateRequestDto);
    	logger.debug("Sim Card created in DB with Id : " + simCard.getId());
    	SimCardCreateResponseDto simCardCreateResponseDto = SimCardCreateResponseDto.getDtoFomEntity(simCard);
    	return new ResponseEntity<SimCardCreateResponseDto>(simCardCreateResponseDto, HttpStatus.OK);
    }
    
    @PostMapping(path="/{simId}/linkToCust/{custId}", consumes = "application/json", produces = "application/json")
    public SimCardLinkResponseDto linkCustomer(@PathVariable Integer simId, 
    							@PathVariable Integer custId) throws CustomerNotFoundException, SimCardNotFoundException, SimCardAlreadyLinkedException {
    	logger.debug("Link Sim Card To Customer API called...");
    	simCardService.linkSimCard(simId, custId);
    	logger.debug("Customer Linked to SimCard Successfully.");
    	SimCardLinkResponseDto simCardLinkResponseDto = new SimCardLinkResponseDto();
    	simCardLinkResponseDto.setStatus(HttpStatus.OK.value());
    	simCardLinkResponseDto.setMessage("Sim Card Linked Successfully.");
    	return simCardLinkResponseDto;
    }
}
