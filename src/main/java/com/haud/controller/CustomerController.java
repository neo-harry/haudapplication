package com.haud.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.haud.dao.Customer;
import com.haud.dto.CustomerCreateRequestDto;
import com.haud.dto.CustomerCreateResponseDto;
import com.haud.dto.CustomerSimCardsResponseDto;
import com.haud.dto.SimCardVo;
import com.haud.services.CustomerService;

@RestController
@RequestMapping(path = "/customer")
public class CustomerController {

	@Autowired
    private CustomerService customerService;
    
	Logger logger = LoggerFactory.getLogger(CustomerController.class);
	
    @PostMapping(path="", consumes = "application/json", produces = "application/json")
    public ResponseEntity<CustomerCreateResponseDto> createCustomer(@Valid @RequestBody CustomerCreateRequestDto customerRequestDto) {
    	logger.debug("Create Customer API called...");
    	Customer customer = customerService.createCustomer(customerRequestDto);
    	logger.debug("Customer created in DB with Id : " + customer.getId());
    	CustomerCreateResponseDto customerCreateResponseDto = CustomerCreateResponseDto.getDtoFomEntity(customer);
    	return new ResponseEntity<CustomerCreateResponseDto>(customerCreateResponseDto, HttpStatus.OK);
    }
    
	@GetMapping(path="/{custId}/simCards", consumes = "application/json", produces = "application/json") 
	public CustomerSimCardsResponseDto getAllSimsByCustId(@PathVariable Integer custId) {
	    logger.debug("Get All Sim Cards Linked to Customer API called...");
	    List<SimCardVo> simList = customerService.getAllSimCardsbyCustId(custId);
	    CustomerSimCardsResponseDto customerSimCardsResponseDto = new CustomerSimCardsResponseDto();
	    if(simList != null && !simList.isEmpty()) {
		    customerSimCardsResponseDto.setStatus(HttpStatus.OK.value());
		    customerSimCardsResponseDto.setData(simList);
	    } else {
		    customerSimCardsResponseDto.setStatus(HttpStatus.BAD_REQUEST.value());
		    customerSimCardsResponseDto.setData("No data found, Please check Inputs."); 
	    } 
	    return customerSimCardsResponseDto; 
    }
	 
}
