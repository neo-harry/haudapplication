package com.haud.exception;

public class CustomerNotFoundException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public CustomerNotFoundException(Integer simId) 
    { 
        super("Customer not found with Id : " + simId + "."); 
    } 
}
