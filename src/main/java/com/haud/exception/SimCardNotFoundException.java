package com.haud.exception;

public class SimCardNotFoundException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public SimCardNotFoundException(Integer simId) 
    { 
        super("Sim Card not found with Id : " + simId + "."); 
    } 
}
