package com.haud.exception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.haud.dto.CommonErrorVo;
import com.haud.dto.ValidationErrorVo;

@ControllerAdvice
public class CustomizedException extends ResponseEntityExceptionHandler {
	
	@Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> details = new ArrayList<>();
        for(ObjectError error : ex.getBindingResult().getAllErrors()) {
            details.add(error.getDefaultMessage());
        }
        ValidationErrorVo error = new ValidationErrorVo(HttpStatus.BAD_REQUEST.value(), "Validation Failed", details);
        return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
    }
	
	@ExceptionHandler(SimCardNotFoundException.class)
	protected ResponseEntity<CommonErrorVo> handleSimCardNotFoundException(SimCardNotFoundException ex) {
		CommonErrorVo response= new CommonErrorVo(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
        return new ResponseEntity<CommonErrorVo>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(CustomerNotFoundException.class)
	protected ResponseEntity<CommonErrorVo> handleCustomerNotFoundException(CustomerNotFoundException ex) {
		CommonErrorVo response= new CommonErrorVo(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
        return new ResponseEntity<CommonErrorVo>(response,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(SimCardAlreadyLinkedException.class)
	protected ResponseEntity<CommonErrorVo> handleSimCardAlreadyLinkedException(SimCardAlreadyLinkedException ex) {
		CommonErrorVo response= new CommonErrorVo(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
        return new ResponseEntity<CommonErrorVo>(response,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
}
