package com.haud.exception;

public class SimCardAlreadyLinkedException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public SimCardAlreadyLinkedException(Integer simId, Integer custId)
    { 
        super("Sim Card with Id " + simId + " can not be linked to Customer with Id " + custId + " because it is already linked to another Customer."); 
    } 
}
