package com.haud.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.haud.dao.SimCard;

@Repository
public interface SimCardRepository extends CrudRepository<SimCard, Integer> {
	
}
