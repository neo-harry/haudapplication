package com.haud.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.haud.dao.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Integer> {
	
	public Customer findByFirstName(String firstName);
	
}
