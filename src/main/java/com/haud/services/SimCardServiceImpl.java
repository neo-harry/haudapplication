package com.haud.services;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.haud.dao.Customer;
import com.haud.dao.SimCard;
import com.haud.dto.SimCardCreateRequestDto;
import com.haud.exception.CustomerNotFoundException;
import com.haud.exception.SimCardAlreadyLinkedException;
import com.haud.exception.SimCardNotFoundException;
import com.haud.repo.CustomerRepository;
import com.haud.repo.SimCardRepository;

@Service
public class SimCardServiceImpl implements SimCardService {
	
	@Autowired
    private ModelMapper modelMapper;
	
	@Autowired
    private SimCardRepository simCardRepository;
	
	@Autowired
    private CustomerRepository customerRepository;
	
	Logger logger = LoggerFactory.getLogger(SimCardServiceImpl.class);
	
	@Override
	public SimCard createSimCard(SimCardCreateRequestDto simCardCreateRequestDto) {
		SimCard simCard = modelMapper.map(simCardCreateRequestDto, SimCard.class);
		simCard = simCardRepository.save(simCard);
		logger.debug("Sim Card Created with Id : " +  simCard.getId());
		return simCard;
	}

	@Override
	public void linkSimCard(Integer simId, Integer custId) throws CustomerNotFoundException, SimCardNotFoundException, SimCardAlreadyLinkedException {
		
		  SimCard simCard = simCardRepository.findById(simId).orElse(null);
		  logger.debug("Sim Card from database : " + simCard);
		  if(simCard == null)
			  throw new SimCardNotFoundException(simId);
		  
		  Customer customer = customerRepository.findById(custId).orElse(null);
		  logger.debug("Customer from database : " + customer);
		  if(customer == null)
			  throw new CustomerNotFoundException(custId);
		  
		  if(simCard.getCustomer() == null) {
			  simCard.setCustomer(customer);
			  simCardRepository.save(simCard);
			  logger.debug("Sim Card Linked.");
		  } else
			  throw new SimCardAlreadyLinkedException(simId, custId);
	}
}
