package com.haud.services;

import java.util.List;

import com.haud.dao.Customer;
import com.haud.dto.CustomerCreateRequestDto;
import com.haud.dto.SimCardVo;

public interface CustomerService {

	public Customer createCustomer(CustomerCreateRequestDto customer);

	public List<SimCardVo> getAllSimCardsbyCustId(Integer custId);
}
