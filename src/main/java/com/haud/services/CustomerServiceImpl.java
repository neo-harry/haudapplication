package com.haud.services;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.haud.dao.Customer;
import com.haud.dto.CustomerCreateRequestDto;
import com.haud.dto.SimCardVo;
import com.haud.repo.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
    private ModelMapper modelMapper;
	
	@Autowired
    private CustomerRepository customerRepository;
	
	Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);
	
	@Override
	public Customer createCustomer(CustomerCreateRequestDto customerCreateRequestDto) {
		Customer customer = modelMapper.map(customerCreateRequestDto, Customer.class);
		customer = customerRepository.save(customer);
		logger.debug("Customer Created with Id : " +  customer.getId());
		return customer;
	}

	@Override
	public List<SimCardVo> getAllSimCardsbyCustId(Integer custId) {
		Customer customer = customerRepository.findById(custId).orElse(null);
		logger.debug("Customer from database : " +  customer);
		List<SimCardVo> simList = null;
	    if(customer != null) {
			  simList = customer.getSimCards().stream().map(e -> { 
				  SimCardVo sim = new SimCardVo();
				  sim.setId(e.getId()); 
				  sim.setImsi(e.getImsi()); 
				  sim.setMsisdn(e.getMsisdn());
				  return sim; 
			  }).collect(Collectors.toList()); 
		}
	    logger.debug("Sim Card List from database : " + simList);
		return simList;
	}

}
