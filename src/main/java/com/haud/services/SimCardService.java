package com.haud.services;

import org.springframework.stereotype.Service;

import com.haud.dao.SimCard;
import com.haud.dto.SimCardCreateRequestDto;
import com.haud.exception.CustomerNotFoundException;
import com.haud.exception.SimCardAlreadyLinkedException;
import com.haud.exception.SimCardNotFoundException;

@Service
public interface SimCardService {

	public SimCard createSimCard(SimCardCreateRequestDto simCardCreateRequestDto);
	
	
	public void linkSimCard(Integer simId, Integer custId) throws CustomerNotFoundException, SimCardNotFoundException, SimCardAlreadyLinkedException;
	
}
