package com.haud.dto;

public class CustomerSimCardsResponseDto {
 
    private Integer status;
    private Object data;
    
    public CustomerSimCardsResponseDto() {
    	
    }

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
   
}
