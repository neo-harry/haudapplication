package com.haud.dto;

import org.modelmapper.ModelMapper;

import com.haud.dao.SimCard;

public class SimCardCreateResponseDto {
 
    private Integer id;
    private Long imsi;
    private Integer msisdn;
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Long getImsi() {
		return imsi;
	}
	public void setImsi(Long imsi) {
		this.imsi = imsi;
	}
	public Integer getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(Integer msisdn) {
		this.msisdn = msisdn;
	}

	public static SimCardCreateResponseDto getDtoFomEntity(SimCard simCard) {
		ModelMapper modelMapper = new ModelMapper();
		SimCardCreateResponseDto simCardCreateResponseDto = modelMapper.map(simCard, SimCardCreateResponseDto.class);
		return simCardCreateResponseDto;
		}
}
