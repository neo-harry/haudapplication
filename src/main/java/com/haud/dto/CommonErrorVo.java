package com.haud.dto;

public class CommonErrorVo {
	
	private Integer status;
	
	public CommonErrorVo(Integer status, String message) {
        this.status = status;
        this.message = message;
    }
 
    public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	//General error message about nature of error
    private String message;
}
