package com.haud.dto;

import javax.validation.constraints.NotEmpty;

public class CustomerCreateRequestDto {
 
	@NotEmpty(message="First Name can not be left blank.")
    private String firstName;
	
	@NotEmpty(message="Last Name can not be left blank.")
	private String lastName;

    public CustomerCreateRequestDto() {
    	
    }

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
