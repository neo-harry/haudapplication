package com.haud.dto;

import java.util.List;

public class ValidationErrorVo {
	
	private Integer status;
	
	public ValidationErrorVo(Integer status, String message, List<String> details) {
     
        this.status = status;
        this.message = message;
        this.details = details;
    }
 
    public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getDetails() {
		return details;
	}

	public void setDetails(List<String> details) {
		this.details = details;
	}

	//General error message about nature of error
    private String message;
 
    //Specific errors in API request processing
    private List<String> details;
}
