package com.haud.dto;

import javax.validation.constraints.NotNull;

public class SimCardCreateRequestDto {
 
	@NotNull(message="IMSI number can not be left blank.")
    private Long imsi;
	
	@NotNull(message="MSISDN number can not be left blank.")
    private Integer msisdn;
    
	public Long getImsi() {
		return imsi;
	}
	public void setImsi(Long imsi) {
		this.imsi = imsi;
	}
	public Integer getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(Integer msisdn) {
		this.msisdn = msisdn;
	}
}
