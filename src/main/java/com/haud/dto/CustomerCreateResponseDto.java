package com.haud.dto;

import org.modelmapper.ModelMapper;

import com.haud.dao.Customer;

public class CustomerCreateResponseDto {
 
    private Integer id;
    private String firstName;
    private String lastName;
    //private List<SimCard> simCards;
 
    public CustomerCreateResponseDto() {
    	
    }
    
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public static CustomerCreateResponseDto getDtoFomEntity(Customer customer) {
		ModelMapper modelMapper = new ModelMapper();
		CustomerCreateResponseDto customerCreateResponseDto = modelMapper.map(customer, CustomerCreateResponseDto.class);
		return customerCreateResponseDto;
	}
	/*
	 * public List<SimCard> getSimCards() { return simCards; }
	 * 
	 * public void setSimCards(List<SimCard> simCards) { this.simCards = simCards; }
	 */
}
