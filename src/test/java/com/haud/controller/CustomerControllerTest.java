package com.haud.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.haud.AbstractTest;
import com.haud.TelecomAppApplication;
import com.haud.dao.Customer;
import com.haud.dto.CustomerCreateRequestDto;
import com.haud.services.CustomerService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelecomAppApplication.class)

public class CustomerControllerTest extends AbstractTest {

	@Autowired
	WebApplicationContext webApplicationContext;
	
	protected MockMvc mvc;
	
	@MockBean
	protected CustomerService customerService;
	
	@Before
	public void setUp() {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void createCustomer() throws Exception {
	   String uri = "/customer";
	   
	   CustomerCreateRequestDto customerCreateRequestDto = new CustomerCreateRequestDto();
	   customerCreateRequestDto.setFirstName("First");
	   customerCreateRequestDto.setLastName("Last");
	   
	   Customer customer = new Customer();
	   customer.setId(1);
	   customer.setFirstName("First");
	   customer.setLastName("Last");
	   
	   String inputJson = super.mapToJson(customer);
	   Mockito.when(customerService.createCustomer(Mockito.any(CustomerCreateRequestDto.class))).thenReturn(customer);
	   MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
	      .contentType(MediaType.APPLICATION_JSON).content(inputJson)).andReturn();
	   
	   int status = mvcResult.getResponse().getStatus();
	   assertEquals(200, status);
	   String content = mvcResult.getResponse().getContentAsString();
	   Customer found = (Customer) super.mapFromJson(content, Customer.class);
	   assertEquals(customer.getId(), found.getId());
	   assertEquals(customer.getFirstName(), found.getFirstName());
	   assertEquals(customer.getLastName(), found.getLastName());
	} 
}
