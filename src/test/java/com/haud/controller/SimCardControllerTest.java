package com.haud.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.haud.AbstractTest;
import com.haud.TelecomAppApplication;
import com.haud.dao.SimCard;
import com.haud.dto.SimCardCreateRequestDto;
import com.haud.dto.SimCardCreateResponseDto;
import com.haud.services.SimCardService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelecomAppApplication.class)

public class SimCardControllerTest extends AbstractTest {

	@Autowired
	WebApplicationContext webApplicationContext;
	
	protected MockMvc mvc;
	
	@MockBean
	protected SimCardService simCardService;
	
	@Before
	public void setUp() {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void createSimCard() throws Exception {
	   String uri = "/sim";
	   SimCardCreateRequestDto simCardCreateRequestDto = new SimCardCreateRequestDto();
	   simCardCreateRequestDto.setImsi(new Long("1234567890123456"));
	   simCardCreateRequestDto.setMsisdn(123456);
	   
	   SimCard simCard = new SimCard();
	   simCard.setId(1);
	   simCard.setImsi(new Long("1234567890123456"));
	   simCard.setMsisdn(123456);
	   
	   String inputJson = super.mapToJson(simCardCreateRequestDto);
	   Mockito.when(simCardService.createSimCard(Mockito.any(SimCardCreateRequestDto.class))).thenReturn(simCard);
	   MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
	      .contentType(MediaType.APPLICATION_JSON).content(inputJson)).andReturn();
	   
		
	   int status = mvcResult.getResponse().getStatus(); 
	   assertEquals(200, status);
	   String content = mvcResult.getResponse().getContentAsString();
	   SimCardCreateResponseDto found = (SimCardCreateResponseDto) super.mapFromJson(content, SimCardCreateResponseDto.class);
	   assertEquals(simCard.getId(), found.getId()); 
	   assertEquals(simCard.getImsi(), found.getImsi()); 
	   assertEquals(simCard.getMsisdn(), found.getMsisdn());	 
	} 
}
