# Haud TelecomApplication
- Project can be run by Running main class HaudApplication.java .
- It will be started on port no. 9091.
- It contains APIs as listed below.

# Customer Creation
	Sample Request
		Method :POST
		URL : http://localhost:9091/customer
		body : {"firstName":"hi2","lastName":"there2"}
		
# Sim Card Creation
	Sample Request
		Method :POST
		URL : http://localhost:9091/sim
		body : {"imsi": 1234567890123456,"msisdn":1234567890}
		
# Link Sim Card to Customer
	Sample Request
		Method :POST
		URL : http://localhost:9091/sim/1/linkToCust/3
		
	[NOTE] : Customer can have many sim cards linked, but one sim card can be linked to one Customer only.
# Get Sim Cards of Customer by customer Id
	Sample Request
		Method :POST
		URL : http://localhost:9091/customer/3/simCards